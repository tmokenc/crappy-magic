#[macro_export]
macro_rules! some_or {
    ($v:expr, $x:expr) => {
        match $v {
            Some(v) => v,
            None => $x,
        }
    };
}

#[macro_export]
macro_rules! ok_or {
    ($v:expr, $x:expr) => {
        match $v {
            Ok(v) => v,
            Err(_) => $x,
        }
    };

    ($v:expr, $err:ident, $x:expr) => {
        match $v {
            Ok(v) => v,
            Err($err) => $x,
        }
    };
}

#[macro_export]
macro_rules! import_all {
    ( $( $x:ident ),+ $(,)?) => {
        $(
            mod $x;
            use self::$x::*;
        )+
    };
}

#[macro_export]
macro_rules! pub_import_all {
    ( $( $x:ident ),+ $(,)?) => {
        $(
            pub mod $x;
            pub use self::$x::*;
        )+
    };
}
