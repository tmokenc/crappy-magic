use std::fmt;

#[allow(non_upper_case_globals)]
pub const Error: MagicError = MagicError;

#[derive(Debug)]
/// A magic error
/// Use this if you don't care about the error value
/// It cannot be casted from any `error` type because itself is an error
/// but it can cast anything that are implemented `Clone` into itself
pub struct MagicError;

impl fmt::Display for MagicError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "This error is a magic. It lives in the fourth dimension and cannot be displayed"
        )
    }
}

impl std::error::Error for MagicError {}

impl<T: Clone> From<T> for MagicError {
    fn from(_: T) -> Self {
        Self
    }
}

#[derive(Debug, Clone, Copy)]
/// The Void
/// This can be use as a return of a function result `fn nope<T>() -> Result<T, Void>`
/// it will cast anything that are implemented `Display` into it, which mean it's able to trigger the `?` operator
/// It self cannot be an `Error`
/// The different from `void` crate is it can occur, but anything will be in the void because it's a Zero Size Type (ZST)
pub struct Void;

impl<T: core::fmt::Display> From<T> for Void {
    fn from(_: T) -> Self {
        Self
    }
}